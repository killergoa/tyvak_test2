/*
 * api_func.c
 *
 *  Created on: Oct 12, 2016
 *      Author: lorenzo
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int i2c_open(void){
	return 0;
}

int i2c_close(void){
	return 0;
}

int i2c_write(char *buf, int size, char device_addr){
	printf("Writing to i2c: %d %d\n", buf[0], buf[1]);
	return 2;
}

int i2c_read(char *buf, int size, char device_addr){
	/* function to return random temperature values, not
	 * really limited to -55 and 125. */
	int posneg = (3.0*rand()/RAND_MAX);
	int someval = (50.0*rand()/RAND_MAX);
	buf[0]=posneg;
	buf[1]=someval;
	return 2;
}
