/*
 ============================================================================
 Name        : Tyvak_T2.c
 Author      : Lorenzo Feruglio
 Version     : 1.0
 Copyright   : 
 Description : Interface to a temperature sensor. As requested, data written
               to the sensor does not take into account the serial address byte
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sensor_api.h>
#include <misc.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	char *p, option, data[2] = { 0 };
	double T_high = 0, T_low = 0;
	int alert = 0, val = 0, counter = 0;

	/* Update sensor config depending on command line arguments */
	if (argc == 4) {
		T_high = strtod(argv[1], NULL);
		T_low = strtod(argv[2], NULL);
		alert = strtol(argv[3], &p, 10);
		update_sensor_config(T_high, T_low, alert);
	} else if (argc == 1) {
		printf("Not updating sensors configuration.\n");
	}

	/* Ask the user for the wanted op_mode */
	option = (char) configure_op_mode();

	/* Open i2c bus */
	val = i2c_open();
	if (val < 0) {
		printf("Error opening i2c bus.\n");
		return EXIT_FAILURE;
	}

	/* Perform either one-shot or continuous mode */
	if (option == 0x00) {
		one_shot_operation(data);
		convert_temperature(data);
	} else {
		continuous_operation();
		while (counter < 1000) {
			read_operation(data);
			convert_temperature(data);
			// Reading sensor 10 times every T reading
			usleep(80000);
			counter++;
		}
	}

	/* Close i2c bus */
	val = i2c_close();
	if (val < 0) {
		printf("Error closing i2c bus.\n");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
