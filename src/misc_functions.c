/*
 * misc_functions.c
 *
 *  Created on: Oct 12, 2016
 *      Author: lorenzo
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sensor_api.h>
#include <misc.h>

int update_sensor_config(double T_high, double T_low, int alert) {
	int val;
	printf("Configuring sensor with new values... ");
	/* Write 0x02 to select the T_high register */
	char buf[2] = { 0x02, 0x00 };
	convert_temp_to_char(T_high, &buf[1]);
	val = i2c_write(buf, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_write: wrote %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};

	/* Write 0x03 to select the T_low register */
	buf[0] = 0x03;
	convert_temp_to_char(T_low, &buf[1]);
	val = i2c_write(buf, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_write: wrote %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};

	/* Write 0x01 to select configuration register.
	 * buf[1] is 01alert00000 */
	buf[0] = 0x01;
	buf[1] = (char) (64 + 32 * alert);
	val = i2c_write(buf, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_write: wrote %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};

	printf("Done.\n");
	return 0;
}

int configure_op_mode() {
	/* Simply ask the user for operation mode choice */
	int option;
	printf("Choose sensor operation.\n0: single, 1: continuous.\n");
	scanf("%d", &option);
	return option;
}

int one_shot_operation(char data[2]) {
	/* Write 0x01 to select the configuration register
	 * Write 11000100 as second character to select one-shot mode and
	 * power-down mode
	 * Read 2 bytes from the Temperature register
	 */
	int val;
	char buf[2] = { 0x01, 0x62 };

	val = i2c_write(buf, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_write: wrote %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};
	/* Sleep 1 second: it is enough since in power-down mode the serial
	 * remains powered and data can be read with no rush. */
	sleep(1);

	val = i2c_read(data, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_read: read %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};

	return 0;
}

int continuous_operation() {
	/* Write 0x00 to select the temperature register
	 * Write 01000000 as second character to select default mode
	 */
	char buf[2] = { 0x00, 0x40 };
	int val;
	val = i2c_write(buf, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_write: wrote %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};
	return 2;
}

int read_operation(char data[2]) {
	/* Read from previously selected sensor. The first byte is the most
	 * significant one, and is assumed to be placed by the driver in position
	 * data[0]. The second byte, the least significant one, is assumed in data[1]. */
	int val;
	val = i2c_read(data, 2, 0x48);
	if (val != 2) {
		printf("Error in i2c_read: read %d bytes instead of 2.\n", val);
		return EXIT_FAILURE;
	};
	return 0;
}

double convert_temperature(char data[2]) {
	/* Convert the temperature. If the recorded number is greater than 511,
	 * then it means that the first digit is a 1, therefore the temperature read
	 * is negative. In this case, we remove the first 1 (subtract 512) and then we
	 * proceed with the formula as per datasheet.
	 */
	int adc_code = (int) data[0] * 256 + (int) data[1];
	double temp;
	if (adc_code > 511) {
		temp = (double) ((adc_code - 512 - 512) / 4);
	} else {
		temp = (double) (adc_code / 4);
	}
	printf("Converted Temp = %lf\n", temp);

	return temp;
}

int convert_temp_to_char(double t_double, char *temp) {
	/* Convert the temperature value from int to char as per datasheet */
	*temp = (char) (t_double * 4 + 512) / 4;
	return 0;
}

