/*
 ============================================================================
 Name        : Tyvak_T2.c
 Author      : Lorenzo Feruglio
 Version     : 1.0
 Copyright   : 
 Description : Interface to a temperature sensor. As requested, data written
               to the sensor does not take into account the serial address byte
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sensor_api.h>
#include <misc.h>
#include <unistd.h>

int main(void) {
	char data[2] = { 0x23, 0x03 };
	double T_high = 0, T_low = 0;
	int alert = 0, val = 0;

	/* Update sensor config depending on command line arguments */
	printf("Testing update_sensor_config...\n");
	T_high = 110.5;
	T_low = -55.4;
	alert = 1;
	if(update_sensor_config(T_high, T_low, alert)!=0){
		printf("\n--> Error in update_sensor_config.\n");
	} else {
		printf("Success.\n");
	}


	/* Open i2c bus */
	printf("Testing i2c_open...\n");
	val = i2c_open();
	if (val < 0) {
		printf("--> Error in i2c_open.\n");
	} else {
		printf("Success.\n");
	}

	/* Test one_shot_operation */
	printf("Testing one_shot_operation...\n");
	if (one_shot_operation(data)!=0){
		printf("\n--> Error in one_shot_operation...\n");
	} else {
		printf("Success.\n");
	}

	/* Testing convert_temperature */
	printf("Testing convert_temperature...\n");
	if(convert_temperature(data)!=-123.0) {
		printf("--> Error in convert_temperature...\n");
	} else {
		printf("Success.\n");
	}

	/* Testing read_operation */
	printf("Testing read_operation...\n");
	if(read_operation(data)!=0) {
		printf("--> Error in read_operation...\n");
	} else {
		printf("Success.\n");
	}

	/* Close i2c bus */
	printf("Testing i2c_close...\n");
	val = i2c_close();
	if (val < 0) {
		printf("--> Error in i2c_close.\n");
	} else {
		printf("Success.\n");
	}
	return EXIT_SUCCESS;
}
