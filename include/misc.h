/*
 * misc.h
 *
 *  Created on: Oct 12, 2016
 *      Author: lorenzo
 */

#ifndef MISC_H_
#define MISC_H_

int update_sensor_config(double T_high, double T_low, int alert);

int configure_op_mode(void);

int one_shot_operation(char data[2]);

int continuous_operation(void);

int read_operation(char data[2]);

double convert_temperature(char data[2]);

int convert_temp_to_char(double t_double, char *temp);

#endif /* MISC_H_ */
