/*
 * sensor_api.h
 *
 *  Created on: Oct 12, 2016
 *      Author: lorenzo
 */

#ifndef SENSOR_API_H_
#define SENSOR_API_H_

int i2c_open(void);

// write buffer
int i2c_write(char *buf, int size, char device_addr);

// read to buffer
int i2c_read(char *buf, int size, char device_addr);

// cleanup
int i2c_close(void);

#endif /* SENSOR_API_H_ */
